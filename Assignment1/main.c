#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

typedef struct Child {
    int pid;
    int fd[2];
} Child;

typedef struct Array {
    int array[128];
    int size;
} Array;

void printIntArray(int array[], int size, int newline, int offset) {
    int index = 0;
    for (index += offset; index < size; index++) {
        printf("%d", array[index]);
        if (index < size - 1) {
            printf(", ");
        }
    }
    if (newline == 0) {
        return;
    } else if (newline == 1) {
        printf("\n");
    }
}

int *castToIntArray(char *array[], int size, int offset) {
    int index;
    int arraySize = size - offset;
    int *newArray = calloc((size_t) arraySize, sizeof(int));
    for (index = 0; index < arraySize; index++) {
        newArray[index] = atoi(array[index + offset]);
    }
    return newArray;
}

int sumIntArray(const int *arr, int size) {
    int sum = 0;
    int i;
    for (i = 0; i < size; i++) {
        sum += arr[i];
    }
    return sum;
}

float ceilf(float n) {
    float baseNumber = (float) (int) n;
    float difference = n - baseNumber;
    return (difference > 0.0) ? baseNumber + 1 : baseNumber;
}

Array *arraySplitter(const int array[], int length, int pieces) {
    float d_result = (float) length / (float) pieces;
    float baseNumber = (int) d_result;
    float finalNumber = ceilf(d_result);

    struct Array *newArray = calloc((size_t) pieces, sizeof(Array));

    int index = 0;
    int incomingArrayIndex = 0;
    for (index = 0; index < pieces; index++) {
        int nextIndex = index + 1;

        int sizeOfNextArray = (nextIndex == pieces) ? (int) finalNumber : (int) baseNumber;
        newArray[index].size = sizeOfNextArray;

        for (int i = 0; i < sizeOfNextArray; ++i) {
            newArray[index].array[i] = array[incomingArrayIndex];
            incomingArrayIndex += 1;
        }
    }
    return newArray;
}

void childProcess(struct Child *children, int size) {
    int me = -1;

    // find out who the hell you are
    for (int i = 0; i < size; i++) {
        if (children[i].pid == 0) {
            me = i;
            break;
        }
    }

    // allocate buffer
    struct Array incomingArray;

    //read data into buffer from the pipe descriptor's write endpoint
    read(children[me].fd[0], &incomingArray, sizeof(Array));

    printf("I am the child with pid: %d, adding the array (size: %d) ", (int) getpid(), incomingArray.size);
    printIntArray(incomingArray.array, incomingArray.size, 0, 0);
    int total = sumIntArray(incomingArray.array, incomingArray.size);
    printf(" and sending partial sum %d.\n", total);

    // write data back out using the same pipe descriptor's read endpoint
    write(children[me].fd[1], &total, sizeof(int));

    //child does not need to keep executing past this point, so terminate
    exit(0);
}

int main(int argc, char *argv[]) {
    //Convert char array to in array
    int offset = 2;
    int *iArray = castToIntArray(argv, argc, offset);
    int childrenCount = atoi(argv[1]);

    printf("I am the parent with pid: %d sending array: ", (int) getpid());
    printIntArray(iArray, argc - offset, 1, 0);

    //get the parent id and declare childPID#'s for later
    int parentPID = (int)getpid();

    struct Child children[childrenCount];
    struct Array *arrays = arraySplitter(iArray, argc - offset, childrenCount);

    //create pipe descriptors based on the amount of children
    for (int i = 0; i < childrenCount; i++) {
        if (parentPID == (int) getpid()) {
            pipe(children[i].fd);
            children[i].pid = fork();
        }
    }

    if ((int) getpid() == parentPID) { // code for the parent
        // send arrays to children
        for (int j = 0; j < childrenCount; ++j) {
            printf("Sending ");
            printIntArray(arrays[j].array, arrays[j].size, 0, 0);
            printf(" to child %d (%d)\n", j + 1, children[j].pid);
            //write data to the pipe descriptor's write endpoint for each child
            write(children[j].fd[1], &arrays[j], sizeof(arrays[j]));
            close(children[j].fd[1]);
        }
    } else {
        childProcess(children, childrenCount);
    }

    // wait for each child to terminate before reading the data they sent
    // tcPID = terminated child PID
    int deadChildren[childrenCount];
    int partialResults[childrenCount];

    for (int j = 0; j < childrenCount; j++) {
        deadChildren[j] = wait(&children[j].pid);
        int partial;
        read(children[j].fd[0], &partial, sizeof(int));
        partialResults[j] = partial;
    }

    //profit (and build the final sentence)
    printf("I am the parent of ");
    for (int k = 0; k < childrenCount; ++k) {
        if (childrenCount == 1) {
            printf("%d", deadChildren[k]);
        } else if ((k + 1) == childrenCount) {
            printf("and %d", deadChildren[k]);
        } else {
            printf("%d, ", deadChildren[k]);
        }
    }
    printf(". I have pid: %d and got partial results ", (int) getpid());
    for (int k = 0; k < childrenCount; ++k) {
        if (childrenCount == 1) {
            printf("%d", partialResults[k]);
        } else if ((k + 1) == childrenCount) {
            printf("and %d", partialResults[k]);
        } else {
            printf("%d, ", partialResults[k]);
        }
    }
    printf(" and the final result is %d.\n\n", sumIntArray(partialResults, childrenCount));

    return 0;
}